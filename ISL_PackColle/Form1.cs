﻿using SharpPcap;
using PacketDotNet;
using ISL.PackColle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISL_PackColle
{
    /// <summary>
    /// メインフォーム
    /// </summary>
    public partial class Form1 : Form
    {
        //変数宣言
        PacketDataManager myPacketDataManager = new PacketDataManager();
        NetMonPacketList packets = new NetMonPacketList();
        Server selectServer;
        NetMonPacket selectPacket;
        public IPAddress userIP;

        ICaptureDevice myDevice;
        public int deviceNo = 0;
        public int adapterNo = 2;

        int frame = 0;

        /// <summary>
        /// フォーム初期化前にデバイス番号の入力フォームの表示
        /// </summary>
        public Form1()
        {
            if (new Welcome(this).ShowDialog() == DialogResult.OK)
                InitializeComponent();
            else Environment.Exit(0);
        }

        /// <summary>
        /// 各種ドライバ等の初期化
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            //PackColleSample.show();
            //netmoninit();
            packets = myPacketDataManager.load();
            if (packets == null) packets = new NetMonPacketList();
            pcapinit();
            this.Visible = true;
            main();
        }

        /// <summary>
        /// SharpPcapに関するメソッド
        /// </summary>
        #region "sharppcap"

        public void pcapinit() 
        {
            //初期化
            myDevice = CaptureDeviceList.Instance[deviceNo];
            PacketNodeManager.setUserIP(adapterNo);

            myDevice.OnPacketArrival += OnPacketArrival;
            myDevice.Open(DeviceMode.Promiscuous, 1000);
            myDevice.StartCapture();

            //フォームを閉じた際、エンジンをクローズする
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit_PCAP);
        }
        
        // イベントハンドラ
        private static void OnPacketArrival(object sender, CaptureEventArgs e)
        {
            PacketDotNet.Packet Tmp_Packet = null;
            PacketDotNet.EthernetPacket Tmp_E = null;
            PacketDotNet.IPv4Packet Tmp_V4 = null;
            TCPPort srcPort = null;
            TCPPort dstPort = null;

        if (e.Packet.LinkLayerType == PacketDotNet.LinkLayers.Ethernet)
        {
            try
                {
                    Tmp_Packet = PacketDotNet.Packet.ParsePacket(e.Packet.LinkLayerType, e.Packet.Data);
                }
                catch (Exception ers)
                {
                    return;
                }
                Tmp_E = (PacketDotNet.EthernetPacket)Tmp_Packet;
             
                if (Tmp_E.Type == PacketDotNet.EthernetPacketType.IpV4)
                {
                    Tmp_V4 = (PacketDotNet.IPv4Packet)Tmp_E.PayloadPacket;
                    IPProtocolType protcol = Tmp_V4.Protocol;

                    if(protcol.ToString().Equals("TCP"))
                    {
                        PacketDotNet.TcpPacket Tmp_tcp = null;
                        Tmp_tcp = PacketDotNet.TcpPacket.GetEncapsulated(Tmp_Packet);
                        srcPort = TCPPortList.getTCPPort(int.Parse(Tmp_tcp.SourcePort.ToString()));
                        dstPort = TCPPortList.getTCPPort(int.Parse(Tmp_tcp.DestinationPort.ToString()));
                    } else if(protcol.ToString().Equals("UDP"))
                    {
                        PacketDotNet.UdpPacket Tmp_udp = null;
                        Tmp_udp = PacketDotNet.UdpPacket.GetEncapsulated(Tmp_Packet);
                        srcPort = TCPPortList.getTCPPort(int.Parse(Tmp_udp.SourcePort.ToString()));
                        dstPort = TCPPortList.getTCPPort(int.Parse(Tmp_udp.DestinationPort.ToString()));
                    }
                    else
                    {
                        srcPort = TCPPortList.getTCPPort(0);
                        dstPort = TCPPortList.getTCPPort(0);
                    }
                    //ホスト名の取得
                    NetMonPacket packet = new NetMonPacket(DateTime.Now.Ticks, ProtcolList.getProtcol(protcol.ToString()), 
                        srcPort, dstPort,  Tmp_Packet.Bytes.Length, 
                        new IPAddress(Tmp_V4.SourceAddress.ToString()),
                        new IPAddress(Tmp_V4.DestinationAddress.ToString()),"http://test.com");
                    Debug.WriteLine(packet);
                    Debug.WriteLine("SrcPort:" + packet.getSrcPort().getPortNum() +
                        "(" + packet.getSrcPort().getPortName() + ")");
                    Debug.WriteLine("DstPort:" + packet.getDstPort().getPortNum() +
                        "(" + packet.getDstPort().getPortName() + ")");
                    PacketNodeManager.add(packet);

                }
            }
        }

        //ApplicationExitイベントハンドラ
        private void Application_ApplicationExit_PCAP(object sender, EventArgs e)
        {
            //キャプチャの中止
            myDevice.StopCapture();
            //キャプチャをクローズ
            myDevice.Close();
            //ApplicationExitイベントハンドラを削除
            Application.ApplicationExit -= new EventHandler(Application_ApplicationExit_PCAP);
            //終了命令
            Environment.Exit(0);
        }

        #endregion

        /// <summary>
        /// 60fpsで駆動させるためのメインメソッド
        /// </summary>
        private void main()
        {
            // 次に処理するフレームの時刻（初回は即処理するので初期値は現在時刻をセット）
            double nextFrame = (double)System.Environment.TickCount;
            // フレームを処理する周期（1/60秒）
            float period = 1000f / 60f;

            // メインループ
            while (this.Visible)
            {
                // 現在の時刻を取得
                double tickCount = (double)System.Environment.TickCount;

                // 次に処理するフレームの時刻まで間がある場合は、処理をスキップする
                if (tickCount < nextFrame)
                {
                    // 1ms以上の間があるか？
                    if (nextFrame - tickCount > 1)
                    {
                        // Sleepする
                        System.Threading.Thread.Sleep((int)(nextFrame - tickCount));
                    }
                    // Windowsメッセージを処理させる
                    Application.DoEvents();
                    // 残りの処理をスキップする
                    continue;
                }

                // 
                // ここで描画以外の計算処理を行う。
                // 
                PacketNodeManager.update();

                // この時点で現在の時刻が次のフレームの時刻より後ならば、
                // このフレームを描画している暇はないので、描画しないでスキップする
                if ((double)System.Environment.TickCount >= nextFrame + period)
                {
                    // Windowsメッセージを処理させる
                    Application.DoEvents();

                    nextFrame += period;
                    frame++;
                    continue;
                }

                // 
                // ここで描画処理を行う。
                // 
                pictureBox1.Refresh();

                // 次のフレームの時刻を計算する
                nextFrame += period;
                frame++;
            }
            Application_ApplicationExit_PCAP(null,null);
        }

        /// <summary>
        /// 描画メソッド
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            //クライアント端末の描画
            int box_length = 50;
            Point center = new Point(this.Width / 2 - box_length / 2, this.Height / 2 - box_length / 2);
            //----
            Image image;
            ImageList.list.TryGetValue(-1, out image);
            if (image != null)
            {
                g.DrawImage(image, new Rectangle(center, new Size(box_length, box_length)));
            }
            else
            {
                g.FillRectangle(Brushes.Black, new Rectangle(center, new Size(box_length, box_length)));
            }
            //----
            //サーバ＆ノード・パケットの描画
            center.X += box_length / 2;
            center.Y += box_length / 2;
            PacketNodeManager.drawNodes(g, center);
            //カーソルを合わせた対象の情報の描画
            PacketNodeManager.drawInfo(g, center, selectServer, selectPacket);
            //ゲット時のメッセージ描画
            PacketNodeManager.drawCaptureMessage(g);
        }

        #region "FormObjectEvents"
        private void button1_Click(object sender, EventArgs e)
        {
            using (Form2 _form = new Form2())
            {
                _form.ShowDialog(this);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (Form3 _form = new Form3())
            {
                _form.ShowDialog(this);
            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            int box_length = 50;
            Point center = new Point(this.Width / 2 - box_length / 2, this.Height / 2 - box_length / 2);
            if(selectPacket==null)
                //サーバ名取得＆設定
                PacketNodeManager.setServerName(selectServer);
            //キャプチャ処理
            int temp = PacketNodeManager.capture(selectPacket, center);
            if (temp >= 0)
            {
                packets.addList(selectPacket);
                TCPPortFlagList.portCapture(temp);
                myPacketDataManager.write(packets);
                selectPacket = null;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            int box_length = 50;
            Point center = new Point(this.Width / 2 - box_length / 2, this.Height / 2 - box_length / 2);
            selectServer = PacketNodeManager.getServer(this.PointToClient(Control.MousePosition), center);
            selectPacket = PacketNodeManager.getPacket(this.PointToClient(Control.MousePosition), center);
        }

        #endregion
    }
}