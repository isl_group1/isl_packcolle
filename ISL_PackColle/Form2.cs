﻿using ISL.PackColle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISL_PackColle
{
    public partial class Form2 : Form
    {
        
        NetMonPacketList loadedPacketList;

        List<NetMonPacket> packetList;

        public Form2()
        {
            InitializeComponent();
            loadedPacketList = new NetMonPacketList();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            init();
        }


        private void init()
        {
            int width = 80;
            int height = 80;

            imageList1.ImageSize = new Size(width, height);

            PacketDataManager packetDataManager = new PacketDataManager();
            loadedPacketList = packetDataManager.load();
            if (loadedPacketList == null) return;
            packetList = loadedPacketList.getNMPacketList();

            listView1.Clear();
            listView1.LargeImageList = imageList1;

            int i = 0;

            //読み込んだデータを表示
            foreach (NetMonPacket packet in packetList)
            {
                try
                {
                    String portName = TCPPortList.getTCPPort(packet.getPort().getPortNum()).getPortName();
                    String imagePath = "image/" + portName + ".png";
                    if (System.IO.File.Exists(imagePath))
                    {
                        //
                    }
                    else
                    {
                        portName = TCPPortList.getTCPPort(packet.getDstPort().getPortNum()).getPortName();
                        imagePath = "image/" + portName + ".png";
                        if(!System.IO.File.Exists(imagePath))
                            imagePath = "image/nai.png";
                    }

                    Image original = Bitmap.FromFile(imagePath);
                    Image thumbnail = createThumbnail(original, width, height);

                    imageList1.Images.Add(thumbnail);
                    listView1.Items.Add(packet.getProtcol().ToString(), i++);

                    original.Dispose();
                    thumbnail.Dispose();
                }
                catch (Exception)
                {
                    return;
                }
            }
        }

        // 幅w、高さhのImageオブジェクトを作成
        Image createThumbnail(Image image, int w, int h)
        {
            Bitmap canvas = new Bitmap(w, h);

            Graphics g = Graphics.FromImage(canvas);
            g.FillRectangle(new SolidBrush(Color.White), 0, 0, w, h);

            float fw = (float)w / (float)image.Width;
            float fh = (float)h / (float)image.Height;

            float scale = Math.Min(fw, fh);
            fw = image.Width * scale;
            fh = image.Height * scale;

            g.DrawImage(image, (w - fw) / 2, (h - fh) / 2, fw, fh);
            g.Dispose();

            return canvas;
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 選択項目があるかどうかを確認する
            if (listView1.SelectedItems.Count == 0)
            {
                // 選択項目がないので処理をせず抜ける
                return;
            }

            // 選択項目を取得する
            ListViewItem itemx = listView1.SelectedItems[0];

            //MessageBox.Show(itemx.Text);
            // ウィンドウハンドルの作成
            Graphics g = Graphics.FromHwnd(this.Handle);

            // イメージリストにあるイメージをフォームに描画する
            //imageList1.Draw(g, new Point(520, 60), itemx.Index);
            index = itemx.Index;
            pictureBox1.Image = imageList1.Images[index];
            label1.Text = "プロトコル      :"    + packetList[index].getProtcol();
            label2.Text = "送信先IPアドレス:"      + packetList[index].getDstIP().ToString();
            label3.Text = "送信先ポート番号:"    + packetList[index].getDstPort().ToString();
            label4.Text = "送信元IPアドレス：" + packetList[index].getSrcIP().ToString();
            label5.Text = "送信元ポート番号：" + packetList[index].getSrcPort().ToString();

        }//test
        int index;

        private void button1_Click(object sender, EventArgs e)
        {
            using (Form4 _form = new Form4())
            {
                try
                {
                    _form.setNetMonPacket(packetList[index]);
                    _form.ShowDialog(this);
                }
                catch
                {
                    return;
                }
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            PacketDataManager packetDataManager = new PacketDataManager();
            packetDataManager.delete(packetList[index].getTime());
            init();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
