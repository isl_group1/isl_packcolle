﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.NetworkMonitor;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;

namespace Network_Monitor_Demo
{
    public struct Constants
    {
        public static UInt32 ADAPTER_INDEX = 2;
    }

    public class Netmon
    {

        public Netmon()
        {

            uint ret;


            //Open a capture file for saving frames.
            string path = @"C:\\Capture\\10sec.cap";

            IntPtr myCapFile;
            uint CapSize;
            ret = NetmonAPI.NmCreateCaptureFile(path, 20000000, NmCaptureFileFlag.WrapAround, 
                out myCapFile, out CapSize);
            if (ret != 0)
            {
                MessageBox.Show("Error Opening Capture File" + "10sec.cap");
                return;
            }

            //Open the capture engine
            IntPtr myCaptureEngine;
            ret = NetmonAPI.NmOpenCaptureEngine(out myCaptureEngine);

            if (ret != 0)
            {
                MessageBox.Show("Error opening capture engine.");
                NetmonAPI.NmCloseHandle(myCapFile);
                return;

            }

            ret = NetmonAPI.NmConfigAdapter(myCaptureEngine, Constants.ADAPTER_INDEX,
                new CaptureCallbackDelegate(FrameIndicationCallback), myCapFile, 
                NmCaptureCallbackExitMode.ReturnRemainFrames);

            if (ret != 0)
            {
                MessageBox.Show("Error configuration adapter.");
                NetmonAPI.NmCloseHandle(myCaptureEngine);
                NetmonAPI.NmCloseHandle(myCapFile);

                return;
            }

            MessageBox.Show("Capturing for 10 seconds.");
            NetmonAPI.NmStartCapture(myCaptureEngine, Constants.ADAPTER_INDEX, NmCaptureMode.Promiscuous);



            Thread.Sleep(10000);


            MessageBox.Show("Stopping Capture.");
            NetmonAPI.NmStopCapture(myCaptureEngine, Constants.ADAPTER_INDEX);
            NetmonAPI.NmCloseHandle(myCaptureEngine);
            NetmonAPI.NmCloseHandle(myCapFile);

            return;

        }


        public void FrameIndicationCallback(IntPtr hCapEng, UInt32 ulAdatIdx, IntPtr pContext, IntPtr hRawFrame)
        {
            uint ret;
            uint rawFrameLength;
            uint actFrameLength;
            ret = NetmonAPI.NmGetRawFrameLength(hRawFrame,out rawFrameLength);

            var rawFrameBytes = new byte[rawFrameLength];
            unsafe
            {
                fixed (byte* rawFrameBuf = &rawFrameBytes[0])
                {
                    ret = NetmonAPI.NmGetRawFrame(hRawFrame, rawFrameLength, rawFrameBuf, out actFrameLength);
                    Debug.Write(Encoding.GetEncoding("UTF-16LE").GetString(rawFrameBytes));
                    Debug.WriteLine("");
                }
            }
        }

    }
}