﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//PackColleで使用する共通クラス Ver1.01
//例外処理はI/O辺りを一通りやったけど、たぶん完全じゃないです
namespace ISL.PackColle
{

#region 使用例

    public static class PackColleSample
    {
        public static void show()
        {
            //---使用例---
            //PackColleSamle.show();で動きます

            //◆パケットを所持するリストを作成してみる
            NetMonPacketList myPacketList = new NetMonPacketList();
            //2つのパケットを挿入
            myPacketList.addList(new NetMonPacket(ProtcolList.getProtcol("HTTP"), TCPPortList.getTCPPort(80),
                0, new IPAddress(123, 45, 67, 89), new IPAddress(98, 765, 43, 21)));
            myPacketList.addList(new NetMonPacket(ProtcolList.getProtcol("UDP"), TCPPortList.getTCPPort(53),
                0, new IPAddress(123, 45, 67, 89), new IPAddress(192, 168, 50, 5)));

            //◆条件に当てはまるパケットを抽出してみる
            //ここでは、送信先IPアドレスが192.168.50.5であるパケットを抽出
            foreach (NetMonPacket nmPacket in 
                myPacketList.getNMPacketList(new IPAddress(192, 168, 50, 5), NetMonPacketList.DST))
            {
                //抽出パケットを出力
                Debug.WriteLine("抽出パケット：" + nmPacket);
            }

            //◆パケットリストを保存してみる
            PacketDataManager myPacketDataManager = new PacketDataManager();
            if (myPacketDataManager.write(myPacketList))
                Debug.WriteLine("myPacketListを書き込みました");
            else return;

            //◆保存したパケットリストを読み込んでみる
            NetMonPacketList loadedPacketList = new NetMonPacketList();
            loadedPacketList = myPacketDataManager.load();
            if (loadedPacketList != null)
            {
                //読み込んだデータを表示
                foreach (NetMonPacket nmPacket in loadedPacketList.getNMPacketList())
                    Debug.WriteLine("記録データを表示：" + nmPacket);
            }

            //◆long型のIPアドレスをx.x.x.xに変換するには？
            IPAddress ip = new IPAddress(3232248325);
            //IPAdressクラスのgetStringIP()で変換できる。
            Debug.WriteLine(ip.getLongIP().ToString() + "は" + ip.getStringIP() + "です");

            //◆全ポート番号の一覧を捕獲フラグ付きで呼び出してみる
            foreach (TCPPortFlag portFlag in TCPPortFlagList.getList())
                Debug.WriteLine("ポート毎の捕獲フラグ情報：" + portFlag);

            //◆80番ポートを捕獲してみる(データの記録は同時に行われる)
            if (TCPPortFlagList.portCapture(80))
                Debug.WriteLine("新たなポート番号がパケモン図鑑にとうろくされます！"); //初回捕獲時のみ動作

            //◆捕獲済みのHTTPポート一覧を表示してみる
            List<TCPPort> capturedList = TCPPortFlagList.searchTCPPorts(ProtcolList.getProtcol("HTTP"));
            //検索した一覧結果を表示
            foreach(TCPPort capturedPort in capturedList)
            {
                Debug.WriteLine("捕獲済みポート："+capturedPort);
            }
        }
    }

#endregion

#region ①基本リソースに関するクラス

    public class Protcol
    {
        private String name;

        //すべてのProtcolは、ProtcolListで定義されるためこのコンストラクタをプログラムで使用してはならない。
        internal Protcol(String str)
        {
            this.name = str;
        }

        public String getName()
        {
            return name;
        }

        public override String ToString()
        {
            return name;
        }

    }

    public static class ProtcolList
    {
        private static List<Protcol> list;

        static ProtcolList()
        {
            initList();
        }

        public static void initList()
        {
            List<String> dat = new List<String>();
            String line;
            try
            {
                using (StreamReader sr = new StreamReader("protcolList.dat", Encoding.GetEncoding("Shift_JIS")))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        dat.Add(line);
                    }
                }
            }
            catch (Exception) { }
            list = new List<Protcol>();
            for (int i = 0; i < dat.Count; i++)
            {
                list.Add(new Protcol(dat[i]));
            }
        }

        public static List<Protcol> getList()
        {
            return list;
        }

        public static Protcol getProtcol(String str)
        {
            foreach (Protcol prt in list)
            {
                if (prt.getName().Equals(str)) return prt;
            }
            return new Protcol("unknown");
        }
    }

    public class TCPPort
    {
        private int portNum;
        private String portName;
        private Protcol protcol;

        //すべてのPortは、後述するPortListで定義されるためこのコンストラクタをプログラムで使用してはならない。
        internal TCPPort(int i, String str, Protcol prt)
        {
            this.portNum = i;
            this.portName = str;
            this.protcol = prt;
        }

        public int getPortNum()
        {
            return portNum;
        }

        public String getPortName()
        {
            return portName;
        }

        public Protcol getProtcol()
        {
            return protcol;
        }

        public override String ToString()
        {
            return portNum.ToString();
        }
    }

    public static class TCPPortList
    {
        private static List<TCPPort> list;

        static TCPPortList()
        {
            initList();
        }

        public static void initList() {
            List<String> dat = new List<String>();
            String line;
            try
            {
                using (StreamReader sr = new StreamReader("portList.dat", Encoding.GetEncoding("Shift_JIS")))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        dat.Add(line);
                    }
                }
            }
            catch (Exception) { }
            list = new List<TCPPort>();
            for (int i = 0; i < dat.Count; i++)
            {
                String[] split = dat[i].Split(',');
                list.Add(new TCPPort(int.Parse(split[0]), split[1], ProtcolList.getProtcol(split[2])));
            }
        }

        public static List<TCPPort> getList()
        {
            return list;
        }

        public static TCPPort getTCPPort(int i)
        {
            foreach (TCPPort port in list)
            {
                if (port.getPortNum() == i) return port;
            }
            return new TCPPort(i, "unknown", ProtcolList.getProtcol("unknown"));
        }
    }

    #endregion

#region ②パケットに関するクラス

    public class Packet
    {
        private Protcol myProtcol;
        private TCPPort mySrcPort;
        private TCPPort myDstPort;
        private long time;

        public Packet(Protcol prt, TCPPort port)
        {
            this.myProtcol = prt;
            this.mySrcPort = port;
            this.myDstPort = TCPPortList.getTCPPort(0);
            this.time = 0;
        }

        public Packet(Protcol prt, TCPPort srcPort, TCPPort dstPort)
        {
            this.myProtcol = prt;
            this.mySrcPort = srcPort;
            this.myDstPort = dstPort;
            this.time = 0;
        }

        public Packet(Protcol prt, TCPPort port, long time)
        {
            this.myProtcol = prt;
            this.mySrcPort = port;
            this.myDstPort = TCPPortList.getTCPPort(0);
            this.time = time;
        }

        public Packet(Protcol prt, TCPPort srcPort, TCPPort dstPort, long time)
        {
            this.myProtcol = prt;
            this.mySrcPort = srcPort;
            this.myDstPort = dstPort;
            this.time = time;
        }

        public Protcol getProtcol()
        {
            return myProtcol;
        }

        public TCPPort getPort()
        {
            return mySrcPort;
        }

        public TCPPort getSrcPort()
        {
            return mySrcPort;
        }

        public TCPPort getDstPort()
        {
            return myDstPort;
        }

        public long getTime()
        {
            return time;
        }

        public String getStringTime()
        {
            String ret="";
            ret += (int)(time / (1000 * 60 * 60));
            time %= 1000 * 60 * 60;
            ret += ":" + (int)(time / (1000 * 60));
            time %= 1000 * 60;
            ret += ":" + (int)(time / 1000) + "." + (int)(time % 1000);

            return ret;
        }

        public void setProtcol(String str)
        {
            myProtcol = ProtcolList.getProtcol(str);
        }

        public void setTime(int i)
        {
            time += (uint)i;
        }

        public override String ToString()
        {
            return time+","+myProtcol + "," + mySrcPort + "," + myDstPort;
        }
    }

    public class NetMonPacket : Packet
    {
        private int bufferSize;
        private IPAddress srcIP;
        private IPAddress dstIP;
        private String connectSiteURL;

        public NetMonPacket(Protcol prt, TCPPort port, int size, IPAddress srcIP, IPAddress dstIP)
            : base(prt, port)
        {
            this.bufferSize = size;
            this.srcIP = srcIP;
            this.dstIP = dstIP;
            this.connectSiteURL = "";
        }

        public NetMonPacket(Protcol prt, TCPPort port, int size, IPAddress srcIP, IPAddress dstIP, String str)
            : base(prt, port)
        {
            this.bufferSize = size;
            this.srcIP = srcIP;
            this.dstIP = dstIP;
            this.connectSiteURL = str;
        }

        public NetMonPacket(long time, Protcol prt, TCPPort srcPort, TCPPort dstPort, int size, IPAddress srcIP, IPAddress dstIP, String str)
            : base(prt, srcPort, dstPort, time)
        {
            this.bufferSize = size;
            this.srcIP = srcIP;
            this.dstIP = dstIP;
            this.connectSiteURL = str;
        }

        public int getSize()
        {
            return bufferSize;
        }

        public IPAddress getSrcIP()
        {
            return srcIP;
        }

        public IPAddress getDstIP()
        {
            return dstIP;
        }

        public String getConnectSiteURL()
        {
            return connectSiteURL;
        }

        public void setConnectSiteURL( String str)
        {
            this.connectSiteURL = str;
        }

        public override String ToString()
        {
            return base.ToString() + "," + bufferSize + "," + srcIP.getLongIP() + "," + dstIP.getLongIP()+","+connectSiteURL;
        }
    }

    public static class GetFlagListManager
    {
        private static List<Packet> packetList;
        private static List<Boolean> isCaptured;

        static GetFlagListManager()
        {
            packetList = new List<Packet>();
            isCaptured = new List<Boolean>();

            //TODO:ここにデータファイルからデータを抽出し、packetListとisCapturedにそれぞれセットするコードを記述

        }

        /// <summary>
        /// 捕獲済みパケット一覧リストを生成する。
        /// </summary>
        /// <param name="prt">取得したいプロトコルの種類。この値はnewで作成してはならない。必ずProtcolListからsearchして取得したオブジェクトを使用すること。</param>
        /// <returns>捕獲済みパケット一覧リスト。</returns>
        public static List<Packet> searchCapturedPackets(Protcol prt)
        {
            List<Packet> ret = new List<Packet>();
            for (int i = 0; i < packetList.Count(); i++)
            {
                if (packetList[i].getProtcol() == prt && isCaptured[i])
                    ret.Add(packetList[i]);
            }
            return ret;
        }
    }

    public class NetMonPacketList
    {
        private static List<NetMonPacket> list;
        public const int SRC = 0;
        public const int DST = 1;

        public NetMonPacketList()
        {
            list = new List<NetMonPacket>();
        }

        public void addList(NetMonPacket nmPacket)
        {
            list.Add(nmPacket);
        }

        public void removeList(NetMonPacket nmPacket)
        {
            list.Remove(nmPacket);
        }

        public void removeList(int i)
        {
            list.RemoveAt(i);
        }

        /// <summary>
        /// アクティブな通信リストからNetMonPacketListを生成する。
        /// </summary>
        /// <param name="IP">対象の発信元/発信先IPアドレス。</param>
        /// <param name="flag">上記IPアドレスの種類で、NetMonPacketList.SRC又はNetMonPacketList.DSTを指定する。</param>
        /// <returns>検索結果のNetMonPacketリスト。</returns>
        public List<NetMonPacket> getNMPacketList(IPAddress ip, int flag)
        {
            List<NetMonPacket> ret = new List<NetMonPacket>();

            foreach (NetMonPacket nmPacket in list)
            {
                switch (flag)
                {
                    case SRC:
                        if (nmPacket.getSrcIP().getLongIP() == ip.getLongIP())
                            ret.Add(nmPacket);
                        break;

                    case DST:
                        if (nmPacket.getDstIP().getLongIP() == ip.getLongIP())
                            ret.Add(nmPacket);
                        break;
                }
            }

            return ret;
        }

        /// <summary>
        /// アクティブな通信リストからNetMonPacketListを生成する。
        /// </summary>
        /// <param name="prt">対象となるプロトコルの種類(ProtcolListからsearchして取得すること)。</param>
        /// <returns>検索結果のNetMonPacketリスト。</returns>
        public List<NetMonPacket> getNMPacketList(Protcol prt)
        {
            List<NetMonPacket> ret = new List<NetMonPacket>();

            foreach (NetMonPacket nmPacket in list)
            {
                if (nmPacket.getProtcol() == prt)
                    ret.Add(nmPacket);
            }

            return ret;
        }

        /// <summary>
        /// アクティブな通信リストからNetMonPacketListを生成する。
        /// </summary>
        /// <param name="prt">対象となるポートの種類(TCPPortListからsearchして取得すること)。</param>
        /// <returns>検索結果のNetMonPacketリスト。</returns>
        public List<NetMonPacket> getNMPacketList(TCPPort port)
        {
            List<NetMonPacket> ret = new List<NetMonPacket>();

            foreach (NetMonPacket nmPacket in list)
            {
                if (nmPacket.getPort() == port)
                    ret.Add(nmPacket);
            }

            return ret;
        }

        //すべてのNetMonPacketを返す
        public List<NetMonPacket> getNMPacketList()
        {
            return list;
        }

        //timeをインデックスとして特定パケットを抽出
        public NetMonPacket getNMPacket(long time)
        {
            foreach(NetMonPacket nmPacket in list)
            {
                if (nmPacket.getTime() == time)
                    return nmPacket;
            }
            return null;
        }

        //timeをインデックスとして、特定パケットを削除
        public bool deleteNMPacket(long time)
        {
            NetMonPacket nmPacket = getNMPacket(time);
            if(nmPacket!=null)
            {
                list.Remove(nmPacket);
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// IPアドレスの表現を行うクラス
    /// </summary>
    public class IPAddress
    {
        long ip;

        /// <summary>
        /// 空(0.0.0.0)のIPアドレスを作成
        /// </summary>
        public IPAddress()
        {
            this.ip = 0;
        }

        /// <summary>
        /// long型の値よりIPアドレスを作成
        /// </summary>
        /// <param name="l">long型のIPアドレス</param>
        public IPAddress(long l)
        {
            this.ip = l;
        }

        /// <summary>
        /// String型の値よりIPアドレスを作成
        /// </summary>
        /// <param name="str">String型のIPアドレス</param>
        public IPAddress(String str)
        {
            String[] split = str.Split('.');
            try
            {
                IPAddress ip = new IPAddress(int.Parse(split[0]), int.Parse(split[1]), int.Parse(split[2]), int.Parse(split[3]));
                this.ip = ip.getLongIP();
            }
            catch (Exception)
            {
                this.ip = 0;
            }
        }

        /// <summary>
        /// 4セグメント表示形式よりIPアドレスを作成
        /// </summary>
        /// <param name="a">セグメント1</param>
        /// <param name="b">セグメント2</param>
        /// <param name="c">セグメント3</param>
        /// <param name="d">セグメント4</param>
        public IPAddress(int a, int b, int c, int d)
        {
            this.ip = (long)(a * (long)Math.Pow(256, 3) + b * (long)Math.Pow(256, 2) + c * 256 + d);
        }

        /// <summary>
        /// 4セグメント形式のIPアドレスを取得
        /// </summary>
        /// <returns></returns>
        public String getStringIP()
        {
            long temp = ip;
            if (temp < 0) return null;
            if (temp > (long)Math.Pow(256, 4)) return null;

            String ret = "";
            for (int i = 3; i >= 1; i--)
            {
                ret += temp / (long)Math.Pow(256, i) + ".";
                temp %= (long)Math.Pow(256, i);
            }
            ret += temp;

            return ret;
        }

        /// <summary>
        /// long型のIPアドレスを取得
        /// </summary>
        /// <returns></returns>
        public long getLongIP()
        {
            return ip;
        }

        public void setIP(int a, int b, int c, int d)
        {
            this.ip = (long)(a * (long)Math.Pow(256, 3) + b * (long)Math.Pow(256, 2) + c * 256 + d);
        }

        public override String ToString()
        {
            return getStringIP();
        }
    }

    #endregion

#region ③各ポート番号の捕獲フラグ管理クラス

    public class TCPPortFlag : TCPPort
    {
        bool flag;

        public TCPPortFlag(int i, String str, Protcol prt, bool flag)
            : base(i, str, prt)
        {
            this.flag = flag;
        }

        public void setFlag(bool flag)
        {
            this.flag = flag;
        }

        public bool isCaptured()
        {
            return flag;
        }

        public override string ToString()
        {
            return base.ToString() + "," + flag;
        }
    }

    public static class TCPPortFlagList
    {
        static List<TCPPortFlag> list;

        static TCPPortFlagList()
        {
            FlagDataManager myFDManager = new FlagDataManager();
            list = myFDManager.load();
        }

        public static List<TCPPortFlag> getList()
        {
            return list;
        }

        public static List<TCPPort> searchTCPPorts(Protcol protcol)
        {
            List<TCPPort> ret = new List<TCPPort>();
            foreach(TCPPortFlag portFlag in list)
            {
                if (portFlag.isCaptured() && portFlag.getProtcol() == protcol)
                    ret.Add(portFlag);
            }
            return ret;
        }

        public static bool portCapture(int i)
        {
            foreach(TCPPortFlag portFlag in list)
            {
                if (portFlag.getPortNum() == i && (!portFlag.isCaptured()))
                {
                    portFlag.setFlag(true);
                    FlagDataManager myFDManager = new FlagDataManager();
                    return myFDManager.write();
                }
            }

            return false;
        }
    }

    #endregion

#region ④各種データの読み込み・書き出しに関するクラス

    public class CSVManager
    {
        protected bool write(String str, CSVData data)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(str, false, Encoding.GetEncoding("Shift_JIS")))
                {
                    sw.Write(data);
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        protected CSVData load(String str)
        {
            CSVData ret = new CSVData();
            String line;
            try
            {
                using (StreamReader sr = new StreamReader(str, Encoding.GetEncoding("Shift_JIS")))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        ret.addRow(line);
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }

            return ret;
        }

        protected class CSVData
        {
            List<List<String>> data;

            public CSVData()
            {
                this.data = new List<List<string>>();
            }

            public void addRow(List<String> list)
            {
                data.Add(list);
            }

            public void addRow(String str)
            {
                List<String> ret = new List<String>();

                String[] split = str.Split(',');
                foreach (String cell in split)
                {
                    ret.Add(cell);
                }

                data.Add(ret);
            }

            public String getCell(int col, int row)
            {
                if (data.Count() < col) return null;
                if (data[col].Count() < row) return null;
                return data[col][row];
            }

            public List<String> getRow(int col)
            {
                if (data.Count() < col) return null;
                return data[col];
            }

            public int getColSize()
            {
                return data.Count();
            }

            public int getRowSize(int col)
            {
                if (getColSize() < col) return 0;
                return data[col].Count();
            }

            public override String ToString()
            {
                String ret = "";
                foreach (List<String> rowData in data)
                {
                    String row = "";
                    foreach (String cell in rowData)
                    {
                        row += cell + ",";
                    }
                    //巻末に付いた余分な,を消す
                    row = row.TrimEnd(',');
                    ret += row + Environment.NewLine;
                }
                //巻末に付いた余分な改行を消す
                ret = ret.TrimEnd('\r', '\n');
                return ret;
            }
        }
    }

    public class FlagDataManager : CSVManager
    {
        public bool create()
        {
            CSVData data = new CSVData();
            foreach(TCPPort port in TCPPortList.getList())
            {
                List<String> row = new List<String>();
                row.Add(port.getPortNum().ToString());
                row.Add(bool.FalseString);
                data.addRow(row);
            }
            return base.write("flagList.dat", data);
        }

        public bool write()
        {
            CSVData data = new CSVData();

            //NetMonPacketListをCSVDataへ変換
            foreach (TCPPortFlag port in TCPPortFlagList.getList())
            {
                data.addRow(port.ToString());
            }

            return base.write("flagList.dat", data);
        }

        public List<TCPPortFlag> load()
        {
            CSVData data = base.load("flagList.dat");
            if (data == null)
            {
                //エラーの場合は再作成の後再度読み込みを試みる
                create();
                if ((data = base.load("flagList.dat")) == null)
                    return null;
            }

            List<TCPPortFlag> ret = new List<TCPPortFlag>();
            //CSVDataをList<TCPPortFlag>へ変換
            try
            {
                for (int i = 0; i < data.getColSize(); i++)
                {
                    List<String> row = data.getRow(i);
                    int portNum = int.Parse(row[0]);
                    TCPPort port = TCPPortList.getTCPPort(portNum);
                    bool flag = (row[1].Equals(bool.TrueString)) ? true : false;
                    TCPPortFlag portFlag = new TCPPortFlag(
                        portNum,
                        port.getPortName(),
                        port.getProtcol(),
                        flag);
                    ret.Add(portFlag);
                }

                return ret;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }

    public class PacketDataManager : CSVManager
    {
        public bool write(NetMonPacketList nmPacketList)
        {
            CSVData data = new CSVData();

            //NetMonPacketListをCSVDataへ変換
            foreach (NetMonPacket nmPacket in nmPacketList.getNMPacketList())
            {
                data.addRow(nmPacket.ToString());
            }

            return base.write("myPacketList.dat", data);
        }

        public NetMonPacketList load()
        {
            NetMonPacketList ret = new NetMonPacketList();
            CSVData data = base.load("myPacketList.dat");
            if (data == null) return null;

            //CSVDataをNetMonPacketListへ変換
            try
            {
                for (int i = 0; i < data.getColSize(); i++)
                {
                    List<String> row = data.getRow(i);
                    NetMonPacket packet = new NetMonPacket(
                        long.Parse(row[0]), //time
                        ProtcolList.getProtcol(row[1]), //protcol
                        TCPPortList.getTCPPort(int.Parse(row[2])), //SrcPort
                        TCPPortList.getTCPPort(int.Parse(row[3])), //DstPort
                        int.Parse(row[4]), //size
                        new IPAddress(long.Parse(row[5])), //SrcIP
                        new IPAddress(long.Parse(row[6])), //DstIP
                        row[7] //ConnectSiteURL
                        );
                    ret.addList(packet);
                }

                return ret;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool delete(long time)
        {
            NetMonPacketList ret = load();
            if (!ret.deleteNMPacket(time))
                return false;
            if (!write(ret)) return false;
            return true;
        }
    }

#endregion

}
