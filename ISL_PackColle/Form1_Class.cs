﻿using PacketDotNet;
using ISL.PackColle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISL_PackColle
{

    /// <summary>
    /// Form1でのみ使用するクラス群(PacketNodeManagerなど)
    /// </summary>


    public static class ImageList
    {
        public static Dictionary<int, Image> list;

        static ImageList()
        {
            list = new Dictionary<int, Image>();
            foreach (TCPPort port in TCPPortList.getList())
            {
                //画像があるかチェック
                String imagePath = "image/" + port.getPortName() + ".png";
                if (System.IO.File.Exists(imagePath))
                {
                    Image image = new Bitmap(imagePath);
                    list.Add(port.getPortNum(), image);
                }
            }
            //----------------------------------------------
                //画像があるかチェック
            String imagePcPath = "image/pc.png";
            if (System.IO.File.Exists(imagePcPath))
            {
                Image image = new Bitmap(imagePcPath);
                list.Add(-1, image);
            }
            //----------------------------------------------
        }
    }

    abstract public class AnimeManager
    {
        bool status;
        int maxframe;
        int nowframe;
        double progress;

        public AnimeManager()
        {
            status = false;
        }

        public void createAnime(double interval)
        {
            status = true;
            nowframe = 0;
            progress = 0;

            // フレームを処理する周期（1/60秒）
            float period = 1000f / 60f;
            maxframe = (int)(interval / period);
        }

        public bool isAnimeMode()
        {
            return status;
        }

        public double getProgress()
        {
            return progress;
        }

        public void nextAnime()
        {
            nowframe++;
            if (nowframe > maxframe)
                deleteAnmime();
            progress = (double)nowframe / maxframe;
        }

        private void deleteAnmime()
        {
            status = false;
        }
    }

    public class Server
    {
        IPAddress address;
        String name;
        Point point;
        double visible;
        double accel;

        public Server(IPAddress adress, String name, Point point)
        {
            this.address = adress;
            this.name = name;
            this.point = point;
            this.visible = 100;
            this.accel = 0.3;
        }

        public String createName()
        {
            return null;
        }

        public IPAddress getAdress()
        {
            return address;
        }

        public String getName()
        {
            return name;
        }

        public Point getPoint()
        {
            return point;
        }

        public double getVisible()
        {
            return visible;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public void next()
        {
            visible -= accel;
            if (visible < 30)
                visible = 30;
        }

        public void gain()
        {
            visible += 30;
            if (visible > 100)
                visible = 100;
            accel *= 0.9;
        }
    }

    public class PacketChild : AnimeManager
    {
        NetMonPacket packet;
        float progress;
        bool isSrc;

        public PacketChild(NetMonPacket packet, bool isSrc)
        {
            this.packet = packet;
            this.isSrc = isSrc;
            if (isSrc)
                progress = 100;
            else
                progress = 0;
            createAnime(1000);
        }

        public NetMonPacket getPacket()
        {
            return packet;
        }

        public bool next()
        {
            if (isSrc)
            {
                progress -= 0.17f;
                if (progress < 0)
                    return true;
                else return false;
            }
            else
            {
                progress += 0.17f;
                if (progress > 100)
                    return true;
                else return false;
            }
        }

        public float getProgress()
        {
            return progress;
        }

        public int getPort()
        {
            if (!isSrc)
            {
                return this.getPacket().getSrcPort().getPortNum();
            }
            else return this.getPacket().getDstPort().getPortNum();
        }

        public void draw(Graphics g, Point center, PacketNode node, int childLength)
        {
            //画像があるかチェック
            Image image;
            ImageList.list.TryGetValue(getPort(), out image);
            if(image!=null) {
                g.DrawImage(image,
                    new Rectangle(
new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (getProgress() * 0.01)),
(int)(center.Y - (center.Y - node.getGuestLocation().Y) * (getProgress() * 0.01))),
new Size(childLength, childLength)));
                return;
            }

            //それ以外
            Color col = Color.Gray;
            switch (getPacket().getProtcol().ToString())
            {
                case "TCP":
                    col = Color.Green;
                    break;

                case "UDP":
                    col = Color.Blue;
                    break;

                case "ICMP":
                    col = Color.Yellow;
                    break;

                case "HTTP":
                    col = Color.Red;
                    break;

                case "HTTPS":
                    col = Color.Pink;
                    break;
            }

            if (isAnimeMode())
            {
                SolidBrush brush = new SolidBrush(Color.FromArgb((int)(base.getProgress() * 255), col));
                g.FillEllipse(brush, new Rectangle(
new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (getProgress() * 0.01)),
(int)(center.Y - (center.Y - node.getGuestLocation().Y) * (getProgress() * 0.01))),
new Size((int)(childLength * base.getProgress()), (int)(childLength * base.getProgress()))));
                nextAnime();
            }
            else
            {
                int alpha = 255;
                if (!isSrc) alpha = getProgress() > 80 ? (int)((100 - getProgress()) / 20 * 255) : 255;
                else alpha = getProgress() < 20 ? (int)((getProgress()) / 20 * 255) : 255;
                SolidBrush brush = new SolidBrush(Color.FromArgb(alpha, col));
                g.FillEllipse(brush, new Rectangle(
new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (getProgress() * 0.01)),
(int)(center.Y - (center.Y - node.getGuestLocation().Y) * (getProgress() * 0.01))),
new Size(childLength, childLength)));
            }
        }

        public void drawTop(Graphics g, Point center, PacketNode node, int childLength)
        {
            SolidBrush brush = new SolidBrush(Color.FromArgb(255, 255, 255, 255));
            g.FillEllipse(brush, new Rectangle(
new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (getProgress() * 0.01)),
(int)(center.Y - (center.Y - node.getGuestLocation().Y) * (getProgress() * 0.01))),
new Size(childLength, childLength)));
        }
    }

    public class PacketNode
    {
        Server guest;
        List<PacketChild> children;

        public PacketNode(IPAddress address, String name, Point point)
        {
            this.guest = new Server(address, name, point);
            this.children = new List<PacketChild>();
        }

        public void addChild(PacketChild child)
        {
            children.Add(child);
        }

        public void next()
        {
            List<PacketChild> eraser = new List<PacketChild>();
            for (int i = 0; i < children.Count(); i++)
            {
                PacketChild child = children[i];
                if (child.next())
                {
                    getServer().gain();
                    children.Remove(child);
                }
            }
            getServer().next();
        }

        public void removeChild(PacketChild child)
        {
            children.Remove(child);
        }

        public Server getServer()
        {
            return guest;
        }

        public IPAddress getGuestIP()
        {
            return guest.getAdress();
        }

        public Point getGuestLocation()
        {
            return guest.getPoint();
        }

        public List<float> getChildrenProgress()
        {
            List<float> progressess = new List<float>();
            foreach (PacketChild child in children)
            {
                progressess.Add(child.getProgress());
            }
            return progressess;
        }

        public List<PacketChild> getChildren()
        {
            return children;
        }
    }

    public static class PacketNodeManager
    {
        public static IPAddress userIP;
        static List<PacketNode> nodes;
        static Random rnd;

        static int captureFrame = -1;
        static PointF capturePoint;

        const int serverLength = 20;
        const int childLength = 14;

        static PacketNodeManager()
        {
            userIP = null;
            nodes = new List<PacketNode>();
            rnd = new Random();
        }

        public static void setUserIP(int n)
        {
            try
            {
                userIP = new IPAddress(System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[n].ToString());
            }
            catch (Exception)
            {
            }
            if (userIP.getLongIP() == 0)
            {
                IPAddress ip = new IPAddress(0);
                if (new enterIp(ip).ShowDialog() == DialogResult.OK)
                    userIP = ip;
            }
            Debug.WriteLine(userIP);
        }

        public static void setServerName(Server server)
        {
            if (server == null) return;
            try
            {
                System.Net.IPHostEntry iphe = System.Net.Dns.GetHostEntry(server.getAdress().ToString());
                String hostName = iphe.HostName;

                lock (nodes)
                {
                    foreach(PacketNode node in nodes) {
                        if (node.getServer()==server)
                        {
                            node.getServer().setName(hostName);
                        }
                    }
                }
            }
            catch (Exception) { }
        }

        public static void add(NetMonPacket packet)
        {
            IPAddress guestIP;
            String protcol = packet.getProtcol().ToString();
            bool isSrc;
            //IPアドレスが無効な値(0.0.0.0)かどうかのチェック
            if ((packet.getSrcIP().getLongIP() == 0) ||
                (packet.getDstIP().getLongIP() == 0)) return;
            //片方がホストIPであることの確認(および、送信/受信フラグの作成)
            if (userIP != null)
            {
                if (packet.getSrcIP().getLongIP() == userIP.getLongIP())
                {
                    guestIP = packet.getDstIP();
                    if (!packet.getDstPort().getProtcol().ToString().Equals("unknown"))
                        protcol = packet.getDstPort().getProtcol().ToString();
                    isSrc = true;
                }
                else if (packet.getDstIP().getLongIP() == userIP.getLongIP())
                {
                    guestIP = packet.getSrcIP();
                    if (!packet.getSrcPort().getProtcol().ToString().Equals("unknown"))
                        protcol = packet.getSrcPort().getProtcol().ToString();
                    isSrc = false;
                }
                else return;
            }
            else return;
            packet.setProtcol(protcol);

            //ノードの追加
            lock (nodes)
            {
                for (int i = 0; i < nodes.Count(); i++)
                {
                    PacketNode node = nodes[i];
                    if (guestIP.getLongIP() == node.getGuestIP().getLongIP())
                    {
                        node.addChild(new PacketChild(packet, isSrc));
                        return;
                    }
                }
                //サーバの追加
                packet.setConnectSiteURL(guestIP.ToString());
                Point position = new Point(rnd.Next(400), rnd.Next(400));
                PacketNode ret = new PacketNode(guestIP, packet.getConnectSiteURL(), position);
                ret.addChild(new PacketChild(packet, isSrc));
                nodes.Add(ret);
            }

        }

        public static Server getServer(Point p, Point center)
        {
            p.X -= (int)(serverLength * 1.5);
            p.Y -= (int)(serverLength * 1.5);
            lock (nodes)
            {
                foreach (PacketNode node in nodes)
                {
                    Point serverPoint = node.getGuestLocation();
                    serverPoint.X -= (int)(serverLength * 1.0);
                    serverPoint.Y -= (int)(serverLength * 1.0);
                    if (Math.Abs(p.X - serverPoint.X) <= serverLength && Math.Abs(p.Y - serverPoint.Y) <= serverLength)
                        return node.getServer();
                }
            }
            return null;
        }

        public static NetMonPacket getPacket(Point p, Point center)
        {
            p.X -= (int)(childLength * 1.5);
            p.Y -= (int)(childLength * 1.5);
            lock (nodes)
            {
                foreach (PacketNode node in nodes)
                {
                    foreach (PacketChild child in node.getChildren())
                    {
                        Point childPoint = new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (child.getProgress() * 0.01)),
                                (int)(center.Y - (center.Y - node.getGuestLocation().Y) * (child.getProgress() * 0.01)));
                        if (Math.Abs(p.X - childPoint.X) <= childLength && Math.Abs(p.Y - childPoint.Y) <= childLength)
                            return child.getPacket();
                    }
                }
            }
            return null;
        }

        public static void update()
        {
            lock (nodes)
            {
                foreach (PacketNode node in nodes)
                {
                    node.next();
                }
            }
        }

        public static void drawNodes(Graphics g, Point center)
        {
            lock (nodes)
            {
                foreach (PacketNode node in nodes)
                {
                    //サーバの描画
                    int guestBoxLength = 30;
                    SolidBrush brush = new SolidBrush(Color.FromArgb((int)(node.getServer().getVisible()*2.55),150,150,200));
                    g.FillRectangle(brush,
                        new Rectangle(node.getGuestLocation(), new Size(guestBoxLength, guestBoxLength)));
                    //ノードの描画
                    g.DrawLine(Pens.Black, center, node.getGuestLocation());
                    //パケットの描画
                    foreach (PacketChild child in node.getChildren())
                    {
                        child.draw(g, center, node, childLength);
                    }
                }
            }
        }

        public static void drawInfo(Graphics g, Point center, Server server, NetMonPacket packet)
        {
            if (packet == null && server == null) return;
            lock (nodes)
            {
                foreach (PacketNode node in nodes)
                {
                    //サーバの説明描画
                    if (node.getServer() == server)
                    {
                        //説明する
                        Point childPoint = server.getPoint();
                        g.DrawString(server.getName(), new Font("メイリオ", 10),
                            new SolidBrush(Color.FromArgb(200, 0, 0, 0)), childPoint);
                    }
                    if (packet == null) continue;

                    foreach (PacketChild child in node.getChildren())
                    {
                        //パケットの説明描画
                        if (child.getPacket() == packet)
                        {
                            //対象パケットを強調表示
                            child.drawTop(g, center, node, childLength);
                            //説明する
                            Point childPoint = new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (child.getProgress() * 0.01)),
                                (int)(center.Y - (center.Y - node.getGuestLocation().Y) * (child.getProgress() * 0.01)));
                            g.DrawString("プロトコル："+packet.getProtcol(), new Font("メイリオ", 10), Brushes.Black, childPoint);
                        }
                    }
                }
            }
        }

        public static void drawCaptureMessage(Graphics g)
        {
            if (captureFrame < 0) return;
            g.FillRectangle(new SolidBrush(Color.FromArgb(255, 255, 255, 255)), new RectangleF(capturePoint, new SizeF(50, 18)));
            g.DrawString("ゲット！", new Font("メイリオ", 10), Brushes.Black, capturePoint);
            captureFrame++;
            if (captureFrame > 40)
                captureFrame = -1;
            capturePoint.Y -= 0.3f;
        }

        public static int capture(NetMonPacket packet, Point center)
        {
            if (packet == null) return -1;
            lock (nodes)
            {
                foreach (PacketNode node in nodes)
                {
                    foreach (PacketChild child in node.getChildren())
                    {
                        if (child.getPacket() == packet)
                        {
                            //捕獲処理
                            capturePoint=new Point((int)(center.X - (center.X - node.getGuestLocation().X) * (child.getProgress() * 0.01)),
                                (int)(center.Y - (center.Y - node.getGuestLocation().Y) * (child.getProgress() * 0.01)));
                            captureFrame = 0;
                            int ret = child.getPort();
                            if (ret == 0 &&
                                child.getPacket().getProtcol().ToString().Equals("ICMP"))
                                ret = 7;
                            node.removeChild(child);
                            return ret;
                        }
                    }
                }
            }
            return -1;
        }
    }
}
