﻿using ISL.PackColle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISL_PackColle
{
    public partial class Form4 : Form
    {
        NetMonPacket nmpacket;
        public Form4()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }
        public void setNetMonPacket(NetMonPacket n)
        {
            nmpacket = n;
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            label1.Text = "ポート番号：　" + nmpacket.getPort().ToString();
            label2.Text = "プロトコル：　"+nmpacket.getProtcol().ToString();
            label3.Text = "ｱﾌﾟﾘｹｰｼｮﾝ ：　" + nmpacket.getProtcol().ToString();
            DateTime dt = new DateTime(nmpacket.getTime());
            label4.Text = dt.ToString("f") +" に\n" + nmpacket.getConnectSiteURL() + "で出会った";

            label5.Text = "送信先IPアドレス:" + nmpacket.getDstIP().ToString();
            label6.Text = "送信先ポート番号:" + nmpacket.getDstPort().ToString();
            label7.Text = "送信元IPアドレス：" + nmpacket.getSrcIP().ToString();
            label8.Text = "送信元ポート番号：" + nmpacket.getSrcPort().ToString();

            String portName = TCPPortList.getTCPPort(nmpacket.getPort().getPortNum()).getPortName();
            String imagePath = "image/" + portName + ".png";
            if (System.IO.File.Exists(imagePath))
            {
                pictureBox1.Image = Bitmap.FromFile(imagePath);
            }
            else
            {
                pictureBox1.Image = Bitmap.FromFile("image/nai.png");
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
    }
}

