﻿using SharpPcap;
using PacketDotNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISL_PackColle
{
    public partial class Welcome : Form
    {
        Form1 form1;
        bool formNG = false;

        public Welcome(Form1 form1)
        {
            this.form1 = form1;
            InitializeComponent();
        }

        private void Welcome_Load_1(object sender, EventArgs e)
        {
            int deviceNo = 0;
            foreach (SharpPcap.WinPcap.WinPcapDevice device in CaptureDeviceList.Instance)
            {
                int adapterNo = 1;
                String ret;
                ret = "◆Device No.[" + deviceNo + "]：\r\n";
                ret += device.Description.ToString() + "\r\n";
                foreach (SharpPcap.LibPcap.PcapAddress addr in device.Addresses)
                {
                    ret += "◇Adapter No.[" + adapterNo + "]：\r\n";
                    ret += addr.ToString() + "\r\n";
                    adapterNo++;
                }

                this.textBox1.Text = ret;
                deviceNo++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                form1.deviceNo = int.Parse(textBox2.Text);
                form1.adapterNo = int.Parse(textBox3.Text);
            }
            catch (Exception)
            {
                MessageBox.Show("無効な値です。");
                formNG = true;
            }
            if ((form1.deviceNo < 0) || (form1.adapterNo < 0))
            {
                MessageBox.Show("0以上を指定してください。");
                formNG = true;
            }
        }

        private void Welcome_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(formNG)
            {
                formNG = false;
                e.Cancel = true;
            }
        }
    }
}
