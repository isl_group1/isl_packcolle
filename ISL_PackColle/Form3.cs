﻿using ISL.PackColle;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ISL_PackColle
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        List<TCPPortFlag> list;

        private void Form3_Load_1(object sender, EventArgs e)
        {
            list = TCPPortFlagList.getList();

            int width = 200;
            int height =200;

            imageList1.ImageSize = new Size(width, height);

            foreach (TCPPortFlag portFlag in list)
            {
                try
                {
                    String imagePath = "";
                    if (portFlag.isCaptured()) {

                        imagePath = "image/" + portFlag.getPortName() + ".png";
                        if (System.IO.File.Exists(imagePath))
                        {
                            //
                        }
                        else
                        {
                            imagePath = "image/nai.png";
                        }
                    }
                    else
                    {
                        imagePath = "image/" + portFlag.getPortName() + "_s.png";
                        if (System.IO.File.Exists(imagePath))
                        {
                            //
                        }
                        else
                        {
                            imagePath = "image/nai_s.png";
                        }
                    }
                    Image original = Bitmap.FromFile(imagePath);
                    Image thumbnail = createThumbnail(original, width, height);

                    imageList1.Images.Add(thumbnail);
                    listBox1.Items.Add(portFlag.getPortNum() +":"+ portFlag.getProtcol());

                    original.Dispose();
                }
                catch (Exception)
                {

                }
            }
        }

        // 幅w、高さhのImageオブジェクトを作成
        Image createThumbnail(Image image, int w, int h)
        {
            Bitmap canvas = new Bitmap(w, h);

            Graphics g = Graphics.FromImage(canvas);
            g.FillRectangle(new SolidBrush(Color.White), 0, 0, w, h);

            float fw = (float)w / (float)image.Width;
            float fh = (float)h / (float)image.Height;

            float scale = Math.Min(fw, fh);
            fw = image.Width * scale;
            fh = image.Height * scale;

            g.DrawImage(image, (w - fw) / 2, (h - fh) / 2, fw, fh);
            g.Dispose();

            return canvas;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            // 選択項目があるかどうかを確認する
            if (listBox1.SelectedItems.Count == 0)
            {
                // 選択項目がないので処理をせず抜ける
                return;
            }
            // 選択項目を取得する
            // Get the currently selected item in the ListBox.
            string curItem = listBox1.SelectedItem.ToString();

            // Find the string in ListBox2.
            int index = listBox1.FindString(curItem);
            Debug.WriteLine("ind;" + index);

            Graphics g = Graphics.FromHwnd(this.Handle);
            
            // イメージリストにあるイメージをフォームに描画する
            
            pictureBox1.Image = imageList1.Images[index];
            label1.Text = "ポート番号　　　　:　" + list[index].getPortNum();
            label2.Text = "アプリケーション名:　" + list[index].getPortName();
            label3.Text = "プロトコル　　　　:　" + list[index].getProtcol();

            
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

    }
}


